import React from 'react';
import axios from 'axios';

export const axiosInstance = axios.create({
  baseURL: 'https://pro-api.coinmarketcap.com/',
  headers: {
    'X-CMC_PRO_API_KEY': 'bd21bc15-b1b6-45da-84eb-c54fda8c43f8',
  },
});

export const getLastPrice = async () => {
  try {
    const res = await axiosInstance.get(
      `v1/cryptocurrency/quotes/latest?slug=bitcoin`,
    );
    return res;
  } catch (error) {
    console.log(error);
    console.log('error');
  }
};

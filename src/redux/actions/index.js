import {CHANGE_PERIOD, UPDATE_PRICE} from '../types';

export const updatePrice = data => {
  return {
    type: UPDATE_PRICE,
    payload: data,
  };
};

export const changePeriod = data => {
  return {
    type: CHANGE_PERIOD,
    payload: data,
  };
};

import {CHANGE_PERIOD, UPDATE_PRICE} from '../types';

const initialState = {
  price: [],
  period: '30000',
};

export const user = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_PRICE:
      return {...state, price: action.payload};
    case CHANGE_PERIOD:
      return {...state, period: action.payload};
    default:
      return state;
  }
};

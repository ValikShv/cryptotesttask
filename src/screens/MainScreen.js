import React, {useEffect} from 'react';
import { SafeAreaView, StyleSheet, Text, View } from "react-native";

import SelectDropdown from 'react-native-select-dropdown';
import moment from 'moment';
import {useDispatch, useSelector} from 'react-redux';

import {getLastPrice} from '../api';
import {changePeriod, updatePrice} from '../redux/actions';
import {timeSelectors, timeSelectorsValue} from '../data';

const MainScreen = () => {
  const dispatch = useDispatch();
  const price = useSelector(store => store.user.price);
  const period = useSelector(store => store.user.period);

  const priceserv = () => {
    getLastPrice().then(res =>
      dispatch(
        updatePrice([
          {
            time: moment(new Date()).format('hh:mm:ss'),
            price: res.data.data[1],
          },
          ...price.slice(0, 9),
        ]),
      ),
    );
  };

  useEffect(() => {
    priceserv();
  }, []);

  useEffect(() => {
    setTimeout(priceserv, Number(period));
  }, [price]);

  return (
    <SafeAreaView style={styles.wrapper}>
      <View style={styles.row}>
        <Text>Интервал сканирования</Text>
        <SelectDropdown
          buttonStyle={{width: 90, height: 20}}
          defaultButtonText={timeSelectors[0]}
          buttonTextStyle={{fontSize: 12}}
          dropdownStyle={{width: 120}}
          data={timeSelectors}
          onSelect={(selectedItem, index) => {
            dispatch(
              changePeriod(
                timeSelectorsValue[
                  timeSelectorsValue.findIndex(
                    element => element.title == selectedItem,
                  )
                ].value,
              ),
            );
          }}
        />
      </View>

      <View style={styles.row}>
        <View style={styles.boxBorder}>
          <Text>Дата/Время</Text>
        </View>
        <View style={styles.boxBorder}>
          <Text>Цена</Text>
        </View>
      </View>

      <View>
        {price.map(item => {
          return (
            <View style={styles.row}>
              <View style={styles.boxBorder}>
                <Text>{item.time}</Text>
              </View>
              <View style={styles.boxBorder}>
                <Text>{item.price.quote.USD.price}</Text>
              </View>
            </View>
          );
        })}
      </View>
      {/*<TouchableOpacity style={{backgroundColor: 'red', width: 100, height: 100}} onPress={()=>{console.log(nu period)}}/>*/}
    </SafeAreaView>

  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    padding: 20,
  },
  row: {
    flexDirection: 'row',
  },
  boxBorder: {
    width: 120,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'black',
    paddingVertical: 5,
  },
});

export default MainScreen;

export const timeSelectors = ['1 min', '30 min', '1 hour'];

export const timeSelectorsValue = [
  {
    title: '1 min',
    value: '60000',
  },
  {
    title: '30 min',
    value: '1800000',
  },
  {
    title: '1 hour',
    value: '3600000',
  },
];

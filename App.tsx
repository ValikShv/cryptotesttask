import React from 'react';

import {Provider} from 'react-redux';

import MainScreen from './src/screens/MainScreen';
import store from './src/redux/stores';

function App(): JSX.Element {
  return (
    <Provider store={store}>
      <MainScreen />
    </Provider>
  );
}

export default App;
